// Users API url: https://jsonplaceholder.typicode.com/users
// Todos API url: https://jsonplaceholder.typicode.com/todos

// Users API url for specific user ids : https://jsonplaceholder.typicode.com/users?id=2302913
// Todos API url for specific user Ids : https://jsonplaceholder.typicode.com/todos?userId=2321392

// Using promises and the `fetch` library, do the following.

// 1. Fetch all the users

function fetchUsers() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((data) => {
      return data.json();
    })
    .then((userDetail) => {
      console.log(userDetail);
      fetchTodos();
    })
    .catch((err) => {
      console.error(err);
    });
}

fetchUsers()

// 2. Fetch all the todos

function fetchTodos() {
  fetch("https://jsonplaceholder.typicode.com/todos")
    .then((data) => {
      return data.json();
    })
    .then((todosDetail) => {
      console.log(todosDetail);
      fetchUserAndTodo();
    })
    .catch((err) => {
      console.error(err);
    });
}



// 3. Use the promise chain and fetch the users first and then the todos.

function fetchUserAndTodo() {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((data) => {
      return data.json();
    })
    .then((userDetail) => {
      console.log(userDetail);

      return fetch("https://jsonplaceholder.typicode.com/todos");
    })
    .then((data) => {
      return data.json();
    })
    .then((todosDetail) => {
      console.log(todosDetail);
    })
    .catch((err) => {
      console.error(err);
    });
}

// 4. Use the promise chain and fetch the users first and then all the details for each user.

// 5. Use the promise chain and fetch the first todo. Then find all the details for the user that is associated with that todo
